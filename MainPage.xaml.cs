﻿using System;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Celebrio
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private async void AppBar_Opened(object sender, object e)
        {
            WebViewBrush wvb = new WebViewBrush();
            wvb.SourceName = "ContentView";
            wvb.Redraw();
            ContentViewRect.Fill = wvb;
            await Task.Delay(100);
            ContentView.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private async void AppBar_Closed(object sender, object e)
        {
            await Task.Delay(100); 
            ContentView.Visibility = Windows.UI.Xaml.Visibility.Visible;
            ContentViewRect.Fill = new SolidColorBrush(Windows.UI.Colors.Transparent);
        }

        private void Desktop_Click(object sender, RoutedEventArgs e)
        {
            ContentView.Navigate(new Uri("http://system.celebriosoftware.com/"));
            BottomAppBar.IsOpen = false;
        }

        private void News_Click(object sender, RoutedEventArgs e)
        {
            ContentView.InvokeScript("eval", new string[] { "celebrio.applications.run('News', 'Zprávy', 'http://system.celebriosoftware.com:80/app/news', { 'image' : 'http://system.celebriosoftware.com:80/images/seniors/desktop/news/120dpi/icon.png' }); " });
            BottomAppBar.IsOpen = false;
        }

        private void Weather_Click(object sender, RoutedEventArgs e)
        {
            ContentView.InvokeScript("eval", new string[] { "celebrio.applications.run('Weather', 'Počasí', 'http://system.celebriosoftware.com:80/app/weather', { 'image' : 'http://system.celebriosoftware.com:80/images/seniors/desktop/weather/120dpi/icon.png' }); " });
            BottomAppBar.IsOpen = false;
        }

        private void ContentViewRect_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
          /*  ContentView.
            ContentView.Tapped(sender, e);*/
        }

        private void Navigate_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button) sender;
            string command = (string)button.Tag;
            ContentView.InvokeScript("eval", new string[] { command });
            BottomAppBar.IsOpen = false;
        }
    }
}